/**
 * Created by Josh on 6/30/2015.
 */

var fileName = "Angry.m3u";

$(document).ready(function(){

    $.ajax({
        url: fileName,
        success: function(data){
            var lines = data.split("\r\n");
            $.each(lines, function(index, value){
                console.log(index + ": " + value);
                $("#data").append(index + ": " + value + "<br/>");
            });
        },
        dataType: "text"
    });
});